#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "Pacman.h"
#include "Entity.h"
#include "Ghost.h"
#include "Cell.h"
using namespace std;

typedef enum objTypes
{
    MazeObj   = 'x',
    PacmanObj = 'P',
    GhostObj = 'G',
    GhostHObj = '-',
} objTypes;

class UsualFactory;

class Game
{
    friend class UsualFactory;
	int width;
	int height;
	std::vector<Entity*> objects;
	std::vector<Ghost*> ghosts;
	std::vector<Cell*> cells;
	Pacman *pacman;

public:

    Game();
//	Game(const char *fName);
    void CreateUsualObject(char objType, int xPos, int yPos, int color);

	std::vector<Entity> getEntities();
	std::vector<Cell> getCells();
	Pacman getPacman();
	void updateGame(float elapsedTime); // изменения координат объектов
	void render(sf::RenderWindow& window) const; // отрисовка
//    ~Game() = default;
    ~Game()
    {
        if (pacman)
            delete pacman;
    }
/*
	void addCells(Cell* cells_arr, int size)
	{
		for(int i=0; i<size; i++)
			cells.emplace_back(new Cell(cells_arr[i]));
	};
*/
};

class AbstractFactory
{
public:
    void createObjects(char objType, int xPos, int yPos, int color)
    {
        switch (objType)
        {
        case MazeObj:
            createMaze();
            break;
        case PacmanObj:
            createPacman(xPos, yPos);
            break;
        case GhostObj:
            createGhost(xPos, yPos, color);
            break;
        case GhostHObj:
            createGhostHouse(xPos, yPos);
            break;
        }
    }
    virtual void createMaze() = 0;
    virtual Pacman* createPacman(int xPos, int yPos) = 0;
    virtual Ghost* createGhost(int xPos, int yPos, int color) = 0;
    virtual void createGhostHouse(int xPos, int yPos) = 0;
};

class UsualFactory : public AbstractFactory
{
public:
    UsualFactory(Game *game)
    {
        gamePtr = game;
    }
    virtual void createMaze();
    virtual Pacman* createPacman(int xPos, int yPos);
    virtual Ghost* createGhost(int xPos, int yPos, int color) { return nullptr; }
    virtual void createGhostHouse(int xPos, int yPos) {};
    Game* gamePtr;
};
