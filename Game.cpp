#include "Game.h"

void UsualFactory::createMaze()
{
    ifstream inp("cells.txt");
    int x, y, size;
    while (!inp.eof())
    {
        inp >> x
            >> y
            >> size;
        gamePtr->cells.emplace_back(new Cell(x, y, size));
    }
};

Pacman* UsualFactory::createPacman(int xPos, int yPos)
{
//    sf::Image pacImage;
//    pacImage.loadFromFile("Pacman.png");
    if (gamePtr->pacman)
        delete gamePtr->pacman;
    return (gamePtr->pacman = new Pacman(100, 100));
}

void Game::CreateUsualObject(char objType, int xPos, int yPos, int color)
{
    UsualFactory* usFactory = new UsualFactory(this);
    usFactory->createObjects(MazeObj, xPos, yPos, color);
    usFactory->createPacman(100, 100);
    delete usFactory;
}

Game::Game() {
    pacman = nullptr;
    // create factory AbstractGhostFactory* ghostFactory;
    // read the text file with maze and positions of entities
    //if x - create wall and add into walls
    //if P - create pacman, add into objects
    //if b/p/i/c - create factories
    //create ghosts and add them into objects and ghosts
    //if . - create PacGum into objects
    //if o - create SuperPacGum
    //if - - create GhostHouse 
}
/*
Game::Game(const char* fName)
{
    UsualFactory *usFactory = new UsualFactory;
    usFactory->createMaze();
    ifstream inp(fName);
    int x, y, size;
    while (!inp.eof())
    {
        inp >> x
            >> y
            >> size;
        cells.emplace_back(new Cell(x, y, size));
    }
}
*/
void Game::updateGame(float elapsedTime) {
    pacman->update(elapsedTime);
}
void Game::render(sf::RenderWindow& window) const {
    for (auto iter = cells.begin(); iter != cells.end(); iter++)
        (*iter)->render(window);
    pacman->render(window);
}


	

