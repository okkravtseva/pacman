#include <SFML/Graphics.hpp>
#include "Game.h"

void handleEvents(sf::RenderWindow& window, Game& game)
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        // ������ �������� ����
        if (event.type == sf::Event::Closed)
        {
            window.close();
        }
/*
        else
        {
            if(event.type == sf::Event::KeyPressed)
                switch (event.key.code)
                {
                case sf::Keyboard::Up:
                    break;
                case sf::Keyboard::Down:
                    break;
                case sf::Keyboard::Left:
                    break;
                case sf::Keyboard::Right:
                    break;
                }
        }
*/
    }
}

void update(sf::Clock& clock, Game& game)
{
    const float elapsedTime = clock.getElapsedTime().asSeconds();
    clock.restart();
    game.updateGame(elapsedTime);
}

void render(sf::RenderWindow& window, const Game& game)
{
    window.clear();
    game.render(window);
    window.display();
}



int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 800), "PacMan Game Clone", sf::Style::Close);
    sf::Clock clock;

/*
    Game game;
    Cell cells[] =
    {
        Cell(5, 5, 10), // Constructor
        Cell(17, 5, 10)
    };

    game.addCells(cells, sizeof(cells)/sizeof(cells[0]));
*/
    Game game;
    game.CreateUsualObject('x', 0, 0, 0);

    while (window.isOpen())
    {
        handleEvents(window, game);
        update(clock, game);
        render(window, game);
    }

    return 0;
}